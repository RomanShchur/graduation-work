package GraduationWork2020.com.ramoz.prod.gw0.config.jwtConfig;

import GraduationWork2020.com.ramoz.prod.gw0.model.User;
import GraduationWork2020.com.ramoz.prod.gw0.model.jwt.JwtRequest;
import GraduationWork2020.com.ramoz.prod.gw0.model.jwt.JwtResponse;
import GraduationWork2020.com.ramoz.prod.gw0.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class JwtAuthenticationController {
  @Autowired
  private AuthenticationManager authenticationManager;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Autowired
  private JwtUserDetailsService userDetailsService;
  @Autowired
  private UserDao userDao;
  @Autowired
  public static JavaMailSender emailSender;

  @RequestMapping(value = "/authenticateUser", method = RequestMethod.POST)
  public ResponseEntity<?> createAuthenticationToken(@RequestBody
    JwtRequest authenticationRequest) throws Exception {
    authenticate(authenticationRequest.getUsername(),
      authenticationRequest.getPassword());
    final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
    final String token = jwtTokenUtil.generateToken(userDetails);
    User authUser = userDao.getByUsername(authenticationRequest.getUsername());
    if (authUser.getIsActivated() == Boolean.TRUE) {
      return ResponseEntity.ok(new JwtResponse(token));
    }
    else {
      return ResponseEntity.status(HttpStatus.FORBIDDEN)
        .body("User account was not verified!");
    }
  }
  private void authenticate(String username, String password) throws Exception {
    try {
      authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    } catch (DisabledException e) {
      throw new Exception("USER_DISABLED", e);
    } catch (BadCredentialsException e) {
      throw new Exception("INVALID_CREDENTIALS", e);
    }
  }
}
