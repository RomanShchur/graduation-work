package GraduationWork2020.com.ramoz.prod.gw0;

import GraduationWork2020.com.ramoz.prod.gw0.properties.FileStorageProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.io.File;
import java.util.List;

@SpringBootApplication
@EnableJpaRepositories("GraduationWork2020.com.ramoz.prod.gw0.*")
@ComponentScan(basePackages = { "GraduationWork2020.com.ramoz.prod.gw0.*" })
@EntityScan("GraduationWork2020.com.ramoz.prod.gw0.*")
@EnableConfigurationProperties({
  FileStorageProperties.class
})
public class Application {
	static Logger logger = LoggerFactory.getLogger(Application.class);
	private static List Directories;
	public static void main(String[] args) {
		ConfigurableApplicationContext context =
			SpringApplication.run(Application.class, args);
		logger.warn("");
    String fileStorage = "../../../../../../../../Files";
    if (new File(fileStorage).mkdir()) {
      logger.warn("Directories created.");
    }
		logger.warn("Program initialization completed.");
	}
}
