package GraduationWork2020.com.ramoz.prod.gw0.model.repository;

import GraduationWork2020.com.ramoz.prod.gw0.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
  List<Comment> findByFileId(@Param("id") Integer id);
  List<Comment> findByUserId(@Param("id") Integer id);
  Boolean deleteByUserId(@Param("id") Integer id);
}
