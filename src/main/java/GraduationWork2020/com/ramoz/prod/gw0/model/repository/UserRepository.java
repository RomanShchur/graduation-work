package GraduationWork2020.com.ramoz.prod.gw0.model.repository;

import GraduationWork2020.com.ramoz.prod.gw0.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
  User findByUsernameLikeIgnoreCase(@Param("username") String username);
  User findByEmail(@Param("email") String email);
//  User findById(@Param("id") Integer id);
  Boolean existsByUsername(String username);
  Boolean existsByEmail(String email);
}
