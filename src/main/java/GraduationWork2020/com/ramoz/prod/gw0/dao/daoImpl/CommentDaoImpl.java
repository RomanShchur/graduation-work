package GraduationWork2020.com.ramoz.prod.gw0.dao.daoImpl;

import GraduationWork2020.com.ramoz.prod.gw0.dao.CommentDao;
import GraduationWork2020.com.ramoz.prod.gw0.model.Comment;
import GraduationWork2020.com.ramoz.prod.gw0.model.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentDaoImpl implements CommentDao {
  @Autowired
  private CommentRepository commentRepo;
  @Override public Comment addComment(Comment comment) {
    Comment savedComment = commentRepo.saveAndFlush(comment);
    return savedComment;
  }
  @Override public List<Comment> getByFileId(Integer id) {
    return commentRepo.findByFileId(id);
  }
  @Override public List<Comment> getByUserId(Integer id) {
    return commentRepo.findByUserId(id);
  }
  @Override public void deleteComment(Integer id) {
    commentRepo.deleteById(id);
  }
  
  @Override public Boolean deleteAllUserComments(Integer id) {
    return commentRepo.deleteByUserId(id);
  }
}
