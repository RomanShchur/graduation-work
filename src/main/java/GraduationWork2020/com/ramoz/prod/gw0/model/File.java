package GraduationWork2020.com.ramoz.prod.gw0.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.List;

@Entity
@Proxy(lazy=false)
@Table(name = "file")
public class File {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", updatable = false, nullable = false)
  private Integer id;
  @Column(name = "file_name", nullable = false)
  private String fileName;
  @Column(name = "file_uri", nullable = false)
  private String fileUri;
  @Column(name = "file_direction")
  private String fileDirection;
//  @ManyToOne
//  @JoinColumn(name="user_id")
//  @ManyToMany(mappedBy = "files", fetch = FetchType.EAGER)
  @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
  @JoinTable(name = "user_directory",
    joinColumns = {
      @JoinColumn(name = "file_id", //referencedColumnName = "id",
        nullable = false, updatable = false)},
    inverseJoinColumns = {
      @JoinColumn(name = "user_id", //referencedColumnName = "id",
        nullable = false, updatable = false)})
  @JsonManagedReference
  private List<User> users;

  @OneToMany(mappedBy = "file")
  @JsonManagedReference
  private List<Comment> comments;

  public File() {
  }
  public File(String fileName, String fileDirection, String fileUri,
    List<User> users) {
    this.fileName = fileName;
    this.fileUri = fileUri;
    this.fileDirection = fileDirection;
    this.users = users;
  }
  public Integer getId() {
    return id;
  }
  public String getFileName() {
    return fileName;
  }
  public String getFileUri() {
    return fileUri;
  }
  public String getFileDirection() {
    return fileDirection;
  }
  public List<Comment> getComments() {
    return comments;
  }
  public List<User> getUsers() {
    return users;
  }

  public void setId(Integer id) {
    this.id = id;
  }
  public void setFileName(String fileName) {
    this.fileName = fileName;
  }
  public void setFolderName(String folderName) {
    this.fileName = folderName;
  }
  public void setFileUri(String fileUri) {
    this.fileUri = fileUri;
  }
  public void setFileDirection(String fileDirection) {
    this.fileDirection = fileDirection;
  }
  public void setComments(List<Comment> comments) {
    this.comments = comments;
  }
  public void setUsers(List<User> users) {
    this.users = users;
  }

  @Override public String toString() {
    return "File{" + "id=" + id + ", fileName='" + fileName + '\''
      + ", fileUri='" + fileUri + '\'' + ", fileDirection='" + fileDirection
      + '\'' + '}';
  }
  public boolean deleteFile() {
    java.io.File f = new java.io.File(this.getFileDirection());
    return  f.delete();
  }
  public boolean updateFile() {
    java.io.File f = new java.io.File(this.getFileDirection());
    return  f.delete();
  }
  /*
    Returns true if null.
   */
  public boolean isNull() {
    return this == null || this.id == null || this.fileName == null
      || this.fileUri == null || this.fileDirection == null;
  }
}
