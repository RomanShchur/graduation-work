package GraduationWork2020.com.ramoz.prod.gw0.model.repository;

import GraduationWork2020.com.ramoz.prod.gw0.model.File;
import GraduationWork2020.com.ramoz.prod.gw0.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FileRepository extends JpaRepository<File, Integer> {
  Optional<File> findByFileName(@Param("name") String folderName);
  File findById(@Param("id") int id);
//  @Query("select t from Test t join User u where u.username = :username")
//  @Query("select f from file f join User u where u.id = :id")
//  List<File> findAllByUserid(@Param("id") Integer id);
  List<File> findByUsers_id(@Param("id") Integer id);
  Boolean deleteByUsers_id(@Param("id") Integer id);
}
