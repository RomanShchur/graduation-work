package GraduationWork2020.com.ramoz.prod.gw0.config.jwtConfig;

import GraduationWork2020.com.ramoz.prod.gw0.dao.UserDao;
import GraduationWork2020.com.ramoz.prod.gw0.model.User;
import GraduationWork2020.com.ramoz.prod.gw0.model.UserDto;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class JwtUserDetailsService implements UserDetailsService {
  @Autowired
  private UserDao userDaoImpl;
  @Autowired
  private PasswordEncoder bcryptEncoder;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = this.userDaoImpl.findUserByUsername(username);
    if (user == null) {
      throw new UsernameNotFoundException("User not found with username: " + username);
    }
    return new User(user
    .getUsername(), user.getPassword(), user.getEmail());
  }
  public User save(UserDto inUser) {
    User newUser = new User();
    newUser.setUsername(inUser.getUsername());
    newUser.setPassword(bcryptEncoder.encode(inUser.getPassword()));
    newUser.setEmail(inUser.getEmail());
    return userDaoImpl.addUser(newUser);
  }
}
