package GraduationWork2020.com.ramoz.prod.gw0.controller;

import GraduationWork2020.com.ramoz.prod.gw0.config.jwtConfig.JwtRequestFilter;
import GraduationWork2020.com.ramoz.prod.gw0.config.jwtConfig.JwtTokenUtil;
import GraduationWork2020.com.ramoz.prod.gw0.dao.CommentDao;
import GraduationWork2020.com.ramoz.prod.gw0.dao.FileDao;
import GraduationWork2020.com.ramoz.prod.gw0.dao.UserDao;
import GraduationWork2020.com.ramoz.prod.gw0.model.Comment;
import GraduationWork2020.com.ramoz.prod.gw0.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class CommentController {
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Autowired
  private JwtRequestFilter jwtRequestFilter;
  @Autowired
  private CommentDao commentDao;
  @Autowired
  private FileDao fileDao;
  @Autowired
  private UserDao userDao;

  @PostMapping(value = "/userCommentFile/{fileid}")
  public ResponseEntity<Boolean> commentFile(@PathVariable("fileid") int fileid,
    @RequestHeader("Authorization") String token, @RequestHeader("comment_text") String commentText,
    HttpServletRequest request, HttpServletResponse response) {

    System.out.println("Got comment: " + commentText);
    User commenter =
      jwtRequestFilter.checkUserRequestToken(token, request, response);
//    String commentText = request.getParameter("comment_text");
    Comment newComent = new Comment(commentText, commenter, fileDao.getByFileId(fileid));
    commentDao.addComment(newComent);
    return ResponseEntity.ok(Boolean.TRUE);
  }
  @PostMapping(value = "/userDeleteComment/{fileid}/{commentId}")
  public ResponseEntity<Boolean> deleteComment(@PathVariable("fileid") int fileid,
    @PathVariable("commentId") int commentId, @RequestHeader("Authorization") String token,
    HttpServletRequest request, HttpServletResponse response) {
    jwtRequestFilter.checkUserRequestToken(token, request, response);
    User commenter =
      jwtRequestFilter.checkUserRequestToken(token, request, response);
    commentDao.deleteComment(commentId);
    return ResponseEntity.ok(Boolean.TRUE);
  }
}
