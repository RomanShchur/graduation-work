package GraduationWork2020.com.ramoz.prod.gw0.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity
@Table(name = "comment")
public class Comment {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", updatable = false, nullable = false)
  private Integer id;
  @Column(name = "comment_text", nullable = false)
  private String text;
  @ManyToOne
  @JoinColumn(name="user_id")
  @JsonBackReference
  private User user;
  @ManyToOne
  @JoinColumn(name="file_id")
  @JsonBackReference
  private File file;

  public Comment() {
  }
  public Comment(String text, User user, File file) {
    this.text = text;
    this.user = user;
    this.file = file;
  }
  public Integer getId() {
    return id;
  }
  public String getText() {
    return text;
  }
  public User getUser() {
    return user;
  }
  public File getFile() {
    return file;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public void setText(String text) {
    this.text = text;
  }
  public void setUser(User user) {
    this.user = user;
  }
  public void setFile(File file) {
    this.file = file;
  }
  @Override public String toString() {
    return "Comment{" + "id=" + id + ", text='" + text + '\'' + ", user=" + user
      + ", file=" + file + '}';
  }
}
