package GraduationWork2020.com.ramoz.prod.gw0.controller;

import GraduationWork2020.com.ramoz.prod.gw0.config.jwtConfig.JwtRequestFilter;
import GraduationWork2020.com.ramoz.prod.gw0.config.jwtConfig.JwtTokenUtil;
import GraduationWork2020.com.ramoz.prod.gw0.service.FileStorageService;
import GraduationWork2020.com.ramoz.prod.gw0.dao.FileDao;
import GraduationWork2020.com.ramoz.prod.gw0.dao.UserDao;
import GraduationWork2020.com.ramoz.prod.gw0.model.File;
import GraduationWork2020.com.ramoz.prod.gw0.model.UploadFileResponse;
import GraduationWork2020.com.ramoz.prod.gw0.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class FileController {
  private static final Logger logger = LoggerFactory.getLogger(FileController.class);
  @Autowired
  private FileStorageService fileStorageService;
  @Autowired
  private FileDao fileDaoImpl;
  @Autowired
  private UserDao userDaoImpl;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Autowired
  private JwtRequestFilter jwtRequestFilter;
  private int userId = 1;
  private java.io.File fileRule;
  @GetMapping("/getAllUserFiles")
  public List<File> getAllUserFiles (@RequestHeader("Authorization") String token, HttpServletRequest request,
    HttpServletResponse response) {
    User owner =
      jwtRequestFilter.checkUserRequestToken(token, request, response);
    return fileDaoImpl.findAllUserFiles(owner.getId());
  }
  @GetMapping("/getUserFile/{id}")
  public File getUserFile(@RequestHeader("Authorization") String token, HttpServletRequest request,
    HttpServletResponse response, @PathVariable("id") int id) {
    User owner =
      jwtRequestFilter.checkUserRequestToken(token, request, response);
    return fileDaoImpl.getByFileId(id);
  }
  @DeleteMapping(value = "/userDeleteFile/{id}")
  public @ResponseBody boolean deleteFile(@PathVariable("id") int id,
    @RequestHeader("Authorization") String token,
    HttpServletRequest request, HttpServletResponse response) {
    jwtRequestFilter.checkUserRequestToken(token, request, response);
    File fileToDelete = fileDaoImpl.getByFileId(id);
    fileDaoImpl.deleteFile(fileToDelete.getId());
    return fileToDelete.deleteFile();
  }
  @PostMapping("/userUploadFile")
  public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file,
    @RequestHeader("Authorization") String token,
    HttpServletRequest request, HttpServletResponse response) {
    String fileName = fileStorageService.storeFile(file);
    String fileDownloadUri =
      ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/").path(fileName).toUriString();
    
    if (!fileDaoImpl.getByFileName(fileName).isPresent()) {
      String fileDirection = "../../../../../../../../../Files" + fileName;
//      String fileDirection = "E:\\GraduationWork\\Files" + fileName;
      User owner =
        jwtRequestFilter.checkUserRequestToken(token, request, response);
      List<User> owners = new ArrayList<>();
      owners.add(owner);
      File dbFile = new File(fileName, fileDirection, fileDownloadUri, owners);
      System.out.println(dbFile.toString());
      fileDaoImpl.addFile(dbFile);
    }
    return new UploadFileResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize());
  }
//  @PostMapping("/userUploadFile")
//  public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file,
//    @RequestHeader("Authorization") String token,
//    HttpServletRequest request, HttpServletResponse response) {
//    String fileName = fileStorageService.storeFile(file);
//    if (fileDaoImpl.getByFileName(fileName).isNull()) {
//
//    }
//    else {
//      String fileDownloadUri =
//        ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/").path(fileName).toUriString();
//      //    String fileDirection = "../../../../../../../../../Files" + fileName;
//      String fileDirection = "E:\\GraduationWork\\Files" + fileName;
//      User owner =
//        jwtRequestFilter.checkUserRequestToken(token, request, response);
//      List<User> owners = new ArrayList<>();
//      owners.add(owner);
//      System.out.println(owner.getUsername());
//      File dbFile = new File(fileName, fileDirection, fileDownloadUri, owners);
//      System.out.println(dbFile.toString());
//      fileDaoImpl.addFile(dbFile);
//      return new UploadFileResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize());
//    }
//  }
  @GetMapping("/downloadFile/{fileName:.+}")
  public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
    Resource resource = fileStorageService.loadFileAsResource(fileName);
    String contentType = null;
    try {
      contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
    } catch (IOException ex) {
      logger.info("Could not determine file type.");
    }
    if(contentType == null) {
      contentType = "application/octet-stream";
    }
    return ResponseEntity.ok()
      .contentType(MediaType.parseMediaType(contentType))
      .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
      .body(resource);
  }
  public Boolean updateFile(@RequestParam("file") MultipartFile file,
    @RequestHeader("Authorization") String token,
    HttpServletRequest request, HttpServletResponse response) {
    return false;
  }
  @PostMapping(value = "/shareFile/{fileId}/{userId}")
  public Boolean shareFile(@PathVariable("fileId") int fileId,
    @PathVariable("userId") int userId,
    @RequestHeader("Authorization") String token,
    HttpServletRequest request, HttpServletResponse response) {
    jwtRequestFilter.checkUserRequestToken(token, request, response);
    File fileToShare = fileDaoImpl.getByFileId(fileId);
    List<User> users = fileToShare.getUsers();
    User userToShare = userDaoImpl.getById(fileId).get();
    users.add(userToShare);
    fileToShare.setUsers(users);
    return Boolean.TRUE;
  }
  @PostMapping(value = "/shareFile/{fileId}/{userName}")
  public Boolean shareFilebyUsername(@PathVariable("fileId") int fileId,
    @PathVariable("userName") String userName,
    @RequestHeader("Authorization") String token,
    HttpServletRequest request, HttpServletResponse response) {
    jwtRequestFilter.checkUserRequestToken(token, request, response);
    File fileToShare = fileDaoImpl.getByFileId(fileId);
    List<User> users = fileToShare.getUsers();
    User userToShare = userDaoImpl.getByUsername(userName);
    users.add(userToShare);
    fileToShare.setUsers(users);
    return Boolean.TRUE;
  }
}
