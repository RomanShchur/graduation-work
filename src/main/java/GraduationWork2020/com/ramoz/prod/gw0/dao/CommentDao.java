package GraduationWork2020.com.ramoz.prod.gw0.dao;

import GraduationWork2020.com.ramoz.prod.gw0.model.Comment;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentDao {
  Comment addComment(Comment comment);
  List<Comment> getByFileId(Integer id);
  List<Comment> getByUserId(Integer id);
  void deleteComment(Integer id);
  Boolean deleteAllUserComments(Integer id);
}
