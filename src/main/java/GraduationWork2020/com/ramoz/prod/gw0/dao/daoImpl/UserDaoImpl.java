package GraduationWork2020.com.ramoz.prod.gw0.dao.daoImpl;

import GraduationWork2020.com.ramoz.prod.gw0.dao.UserDao;
import GraduationWork2020.com.ramoz.prod.gw0.model.User;
import GraduationWork2020.com.ramoz.prod.gw0.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDaoImpl implements UserDao {
  @Autowired
  private UserRepository userRepo;
  private BCryptPasswordEncoder bCryptPasswordEncoder;
  @Autowired
  public UserDaoImpl(UserRepository userRepo,
    BCryptPasswordEncoder bCryptPasswordEncoder) {
    this.userRepo = userRepo;
    this.bCryptPasswordEncoder = bCryptPasswordEncoder;
  }

  @Override
  public User addUser(User user) {
    user.setIsActivated(false);
    User savedUser = userRepo.saveAndFlush(user);
    return savedUser;
  }
  @Override
  public User getByUsername(String username) {
    return userRepo.findByUsernameLikeIgnoreCase(username);
  }
  @Override
  public User getByEmail(String email) {
    return userRepo.findByEmail(email);
  }
  @Override
  public User editUser(User user) {
    return userRepo.saveAndFlush(user);
  }
  @Override
  public void deleteUser(Integer id) {
    userRepo.deleteById(id);
  }
  @Override
  public Optional<User> getById(Integer id) {
    return userRepo.findById(id);
  }
  @Override
  public User findUserByUsername(String username) {
    return userRepo.findByUsernameLikeIgnoreCase(username);
  }
}
