package GraduationWork2020.com.ramoz.prod.gw0.dao;

import GraduationWork2020.com.ramoz.prod.gw0.model.File;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface FileDao {
  File addFile(File directory);
  List<File> getAll();
  File getByFileId(int id);
  Optional<File> getByFileName(String fileName);
  File editFile(File directory);
  List<File> findAllUserFiles(Integer id);
  void deleteFile(Integer id);
  Boolean deleteAllUserFiles(Integer id);
}
