package GraduationWork2020.com.ramoz.prod.gw0.dao;

import GraduationWork2020.com.ramoz.prod.gw0.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import GraduationWork2020.com.ramoz.prod.gw0.model.repository.UserRepository;

import java.util.Optional;

//@Service
public interface UserDao {
  User addUser(User user);
  User getByUsername(String username);
  User getByEmail(String email);
  Optional<User> getById(Integer id);
  User editUser(User user);
  void deleteUser(Integer id);
  User findUserByUsername(String username);
}
