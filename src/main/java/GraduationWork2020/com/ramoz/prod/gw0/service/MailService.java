package GraduationWork2020.com.ramoz.prod.gw0.service;

import GraduationWork2020.com.ramoz.prod.gw0.model.Mail;

public interface MailService {
  public void sendEmail(Mail mail);
}
