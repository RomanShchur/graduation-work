package GraduationWork2020.com.ramoz.prod.gw0.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "user")
public class User implements UserDetails {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", updatable = false, nullable = false)
  private Integer id;
  @NotBlank
  @Column(name = "username", nullable = false)
  private String username;
  @NotBlank
  @Column(name = "password", nullable = false)
  private String password;
  @NotBlank
  @Column(name = "email", nullable = false)
  @Email
  private String email;
  @Column(name = "is_activated")
  private Boolean isActivated;
//  @OneToMany(mappedBy = "user")
//  @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
//  @JoinTable(name = "user_directory",
//    joinColumns = {
//      @JoinColumn(name = "user_id", referencedColumnName = "id",
//        nullable = false, updatable = false)},
//    inverseJoinColumns = {
//      @JoinColumn(name = "file_id", referencedColumnName = "id",
//        nullable = false, updatable = false)})
  @ManyToMany(mappedBy = "users", fetch = FetchType.LAZY)
  @JsonBackReference
  private List<File> files;

  @OneToMany(mappedBy = "user")
  @JsonManagedReference
  private List<Comment> comments;

  public User() {
  }
  public User(String username, String password, String email) {
    this.username = username;
    this.password = password;
    this.email = email;
  }
  public User(String username, String password) {
    this.username = username;
    this.password = password;
  }
  public Integer getId() {
    return id;
  }
  public String getUsername() {
    return username;
  }
  public String getPassword() {
    return password;
  }
  public String getEmail() {
    return email;
  }
  public Boolean getIsActivated() {
    return isActivated;
  }
  public List<Comment> getComments() {
    return comments;
  }

  public void setId(Integer id) {
    this.id = id;
  }
  public void setUsername(String username) {
    this.username = username;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public void setIsActivated(Boolean isActivated) {
    this.isActivated = isActivated;
  }
  public void setComments(List<Comment> comments) {
    this.comments = comments;
  }

  @Override public String toString() {
    return "User{" + "id=" + id + ", username='" + username + '\''
      + ", password='" + password + '\'' + ", email='" + email + '\'' + '}';
  }
  @Override public boolean isAccountNonExpired() {
    return true;
  }
  @Override public boolean isAccountNonLocked() {
    return true;
  }
  @Override public boolean isCredentialsNonExpired() {
    return true;
  }
  @Override public boolean isEnabled() {
    return true;
  }
  @Override public Collection<? extends GrantedAuthority> getAuthorities() {
    return null;
  }
}
