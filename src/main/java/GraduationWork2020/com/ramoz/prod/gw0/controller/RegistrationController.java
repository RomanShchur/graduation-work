package GraduationWork2020.com.ramoz.prod.gw0.controller;

import GraduationWork2020.com.ramoz.prod.gw0.config.jwtConfig.JwtTokenUtil;
import GraduationWork2020.com.ramoz.prod.gw0.config.jwtConfig.JwtUserDetailsService;
import GraduationWork2020.com.ramoz.prod.gw0.dao.UserDao;
import GraduationWork2020.com.ramoz.prod.gw0.model.User;
import GraduationWork2020.com.ramoz.prod.gw0.model.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class RegistrationController {
  @Autowired
  private AuthenticationManager authenticationManager;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Autowired
  private JwtUserDetailsService userDetailsService;
  @Autowired
  private UserDao userDao;
  @Autowired
  public static JavaMailSender emailSender;

  @RequestMapping(value = "/registerNewUser", method = RequestMethod.POST)
  public ResponseEntity<?> saveUser(@RequestBody UserDto user) throws Exception {
    System.out.println("new user registered.");
//    composeMail(user);
    return ResponseEntity.ok(userDetailsService.save(user));
  }
  @RequestMapping(value = "/verificateUserMail/{username}", method =
    RequestMethod.GET)
  public ResponseEntity<?> verificateMail(@PathVariable String username) throws Exception {
    User activateUser = userDao.getByUsername(username);
    activateUser.setIsActivated(Boolean.TRUE);
    userDao.editUser(activateUser);
    return ResponseEntity.ok("");
  }
  /*
    This method works slowly and not every time.
    Deactivated yet.
   */
  public void composeMail(UserDto user) {
    SimpleMailMessage mail = new SimpleMailMessage ();
    mail.setFrom("ramoztest1@gmail.com");
    mail.setTo(user.getEmail());
    mail.setSubject("Please verify your email");
    mail.setText("Verification link:"
      + "\n\nlink"
      + "\n http://localhost:9850/verificateUserMail/"+ user.getUsername() +"");
    emailSender.send(mail);
  }
}
