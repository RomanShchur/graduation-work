package GraduationWork2020.com.ramoz.prod.gw0.dao.daoImpl;

import GraduationWork2020.com.ramoz.prod.gw0.dao.FileDao;
import GraduationWork2020.com.ramoz.prod.gw0.dao.UserDao;
import GraduationWork2020.com.ramoz.prod.gw0.model.File;
import GraduationWork2020.com.ramoz.prod.gw0.model.User;
import GraduationWork2020.com.ramoz.prod.gw0.model.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FileDaoImpl implements FileDao {
  @Autowired
  private FileRepository fileRepo;
  @Autowired
  private UserDao userDao;
  @Override
  public File addFile(File file) {
    File savedDir = fileRepo.saveAndFlush(file);
    return savedDir;
  }
  @Override
  public List<File> getAll() {
    return fileRepo.findAll();
  }
  @Override
  public Optional<File> getByFileName(String filename) {
    return fileRepo.findByFileName(filename);
  }
  @Override
  public File getByFileId(int id) {
    return fileRepo.findById(id);
  }
  public void deleteFile(File file) {
    fileRepo.delete(file);
  }
  @Override
  public File editFile(File file) {
    return fileRepo.saveAndFlush(file);
  }
  @Override
  public List<File> findAllUserFiles(Integer id) {
//    User user = userDao.getById(id).get();
    return fileRepo.findByUsers_id(id);
  }
  @Override
  public void deleteFile(Integer id) {
    fileRepo.deleteById(id);
  }
  
  @Override public Boolean deleteAllUserFiles(Integer id) {
    return fileRepo.deleteByUsers_id(id);
  }
}
