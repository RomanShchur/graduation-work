package GraduationWork2020.com.ramoz.prod.gw0.controller;

import GraduationWork2020.com.ramoz.prod.gw0.config.jwtConfig.JwtRequestFilter;
import GraduationWork2020.com.ramoz.prod.gw0.dao.CommentDao;
import GraduationWork2020.com.ramoz.prod.gw0.dao.FileDao;
import GraduationWork2020.com.ramoz.prod.gw0.dao.UserDao;
import GraduationWork2020.com.ramoz.prod.gw0.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserController {
  @Autowired
  private FileDao fileDao;
  @Autowired
  private UserDao userDao;
  @Autowired
  private CommentDao commentDao;
  @Autowired
  private JwtRequestFilter jwtRequestFilter;
  @DeleteMapping(value = "/deleteUser")
  public @ResponseBody Boolean deleteFile(@RequestHeader("Authorization")
    String token, HttpServletRequest request, HttpServletResponse response) {
    User userToDelete =
      jwtRequestFilter.checkUserRequestToken(token, request, response);
    commentDao.deleteAllUserComments(userToDelete.getId());
    fileDao.findAllUserFiles(userToDelete.getId());
    userDao.deleteUser(userToDelete.getId());
    return true;
  }
}
