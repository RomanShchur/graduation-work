package GraduationWork2020.com.ramoz.prod.gw0.service;

import GraduationWork2020.com.ramoz.prod.gw0.dao.UserDao;
import GraduationWork2020.com.ramoz.prod.gw0.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class MyUserDetailsService implements UserDetailsService {
  @Autowired
  private UserDao udaoImpl;
  @Override
  @Transactional
  public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
    User user = udaoImpl.getByUsername(userName);
    return buildUserForAuthentication(user);
  }
  private UserDetails buildUserForAuthentication(User user) {
    return new User(user.getUsername(), user.getPassword());
  }
}
