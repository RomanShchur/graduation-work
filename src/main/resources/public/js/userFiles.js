(function createTableFromJSON(){
  var xhr = new XMLHttpRequest();
  var url = "http://localhost:9800/getAllUserFiles";
  xhr.open("GET", url, true);
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.setRequestHeader("Authorization", "Bearer " +
   sessionStorage.getItem("accessToken"));
  xhr.send();
  xhr.onload = function(event) {
    var files = JSON.parse(this.response);
    var tableBody =
    '<table width="100%" cellpadding="3" align="center"'+
      'style="font-size:12px;border-collapse:collapse;" border="1">'+
    '<tr style="font-weight:bold;background:#0275d8;">'+
    //'<td style="color:white;">File ID</td>'+
    '<td style="color:white;">File</td>'+
    '<td style="color:white;">DownloadLink</td>'+
    //'<td style="color:white;">Delete</td>
    '</tr>';
    files.forEach(function(file) {
    console.log(file.fileName);
      tableBody +=
      '<tr>'+
        //'<td>'+d.id+'</td>'+
        '<td><a href = http://localhost:9800/userFilePreview.html?id='+file.id+'>'+file.fileName+'</a></td>'+
        '<td><a href = ' + file.fileUri + '>Download</td>'+
        //'<td><a href = "#"  onclick="sendDeleteFile(' +d.id+ ');' +
        //'return false;">Delete</td>'+
      '</tr>';
    });
    tableBody += '<table>';
    // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
    var divContainer = document.getElementById("showData");
    divContainer.innerHTML = tableBody;
  }
  error : function errorGeneratingTable1(result) {
    //handle the error
   }
})();
function sendDeleteFile(id) {
  if (window.confirm("Are you sure you want to delete file?")) {
    var xhr = new XMLHttpRequest();
    var urlDel = "http://localhost:9800/userDeleteFile/"+id;
    xhr.open("DELETE", urlDel, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Authorization", "Bearer " +
        sessionStorage.getItem("accessToken"));
    xhr.send();
  }
  else {
  }
}

