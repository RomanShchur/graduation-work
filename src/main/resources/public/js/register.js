function sendRegisterJSON() {
  let userName = document.getElementById('inputUsername1').value;
  let userEmail = document.getElementById("inputEmail1").value;
  let userPassword = document.getElementById("inputPassword1").value;
  // Creating a XHR object
  let xhr = new XMLHttpRequest();
  let url = "submit.php";
  // open a connection
  xhr.open("POST", url, true);
  // Set the request header i.e. which type of content you are sending
  xhr.setRequestHeader("Content-Type", "application/json");
  // Create a state change callback
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      // Print received data from server
      result.innerHTML = this.responseText;
    }
  };
  // Converting JSON data to string
  var data = JSON.stringify({ "username": userName.value, "userEmail": userEmail.value, "userPassword": userPassword.value });
  // Sending data with the request
  xhr.send(data);
}
