function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
    vars[key] = value;
  });
  return vars;
}

(function readFileData(){
  var xhr = new XMLHttpRequest();
  var url = "http://localhost:9800/getUserFile/" + getUrlVars()["id"];
  xhr.open("GET", url, true);
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.setRequestHeader("Authorization",
   "Bearer " + sessionStorage.getItem("accessToken"));
  xhr.send();
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var json = JSON.parse(xhr.responseText);
    }
  };
  xhr.onload = function(event) {
    var file = JSON.parse(xhr.response);
    var fileNameElem = document.getElementById("filename");
    fileNameElem.textContent = file.fileName;

    setButtonActive(file);

    sessionStorage.setItem('fileDownloadUri', file.fileUri);
    sessionStorage.setItem('fileId', file.id);
    var tableBody =
    '<table width="100%" cellpadding="3" align="center"'+
      'style="font-size:12px;border-collapse:collapse;" border="0">'+
    '<tr style="font-weight:bold;background:#0275d8;">'+
//     '<td style="color:white;">File ID</td>'+
//     '<td style="color:white;">commentId</td>'+
      '<td style="color:white;">Comments</td>'+
//     '<td style="color:white;">Delete</td>'+
    '</tr>';
    file.comments.forEach(function(comment) {
      tableBody +=
      '<tr>'+
//        '<td>'+comment.id+'</td>'+
//        '<td>' + comment.user.username + '</a></td>'+
        '<td>' + comment.text + '</td>'+
//        '<td><a href = "#"  onclick="sendDeleteFile(' +comment.id+ ');'
//        'return false;">Delete</td>'
      '</tr>';
    });
    tableBody += '<table>';
    // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
    var divContainer = document.getElementById("showComments");
    divContainer.innerHTML = tableBody;
  }
  error : function errorGeneratingTable1(result) {
    //handle the error
   }
})();
function sendDeleteFile() {
  if (window.confirm("Are you sure you want to delete file?")) {
    var xhr = new XMLHttpRequest();
    var urlDel = "http://localhost:9800/userDeleteFile/"+
      sessionStorage.getItem('fileDownloadUri');
    xhr.open("DELETE", urlDel, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Authorization", "Bearer " +
        sessionStorage.getItem("accessToken"));
    xhr.send();
  }
  else {
  }
}
function addUserToFile() {
  var sxhr = new XMLHttpRequest();
  var sUserName = document.getElementById("shareUserName");
  var urlShare = "http://localhost:9800/shareFile/"+
    sessionStorage.getItem('fileId');+"/"+sUserName;
  sxhr.open("POST", urlShare, true);
  sxhr.setRequestHeader("Content-Type", "application/json");
  sxhr.setRequestHeader("Authorization", "Bearer " +
       sessionStorage.getItem("accessToken"));
  sxhr.send();
}
function userCommentFile() {
  var cxhr = new XMLHttpRequest();
  var commentText = document.getElementById("CommentArea1");
  window.alert(commentText);
  var urlComment = "http://localhost:9800/userCommentFile/"+
    sessionStorage.getItem('fileId');
  cxhr.open("POST", urlComment, true);
  cxhr.setRequestHeader("Content-Type", "application/json");
  cxhr.setRequestHeader("Authorization", "Bearer " +
       sessionStorage.getItem("accessToken"));
  cxhr.setRequestHeader("comment_text", commentText);
  cxhr.send();
}
function setButtonActive(file) {
    var fileDownliad = document.getElementById("downloadLink");
    fileDownliad.setAttribute("href", file.fileUri);
    fileDownliad.setAttribute("class", "btn btn-primary");
    fileDownliad.setAttribute("aria-disabled", "false");
}
